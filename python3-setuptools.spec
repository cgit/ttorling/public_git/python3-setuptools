# This package is for EPEL only
# Dependencies for check and wheel introduce circular dependencies
# Set this to 0 after we've bootstrapped.
%{!?_with_bootstrap: %global bootstrap 1}

%if ! 0%{?bootstrap}
%global with_check 1
%global build_wheel 1
%else
%global with_check 0
%global build_wheel 0
%endif

%global srcname setuptools
%if 0%{?build_wheel}
%global python3_wheelname %{srcname}-%{version}-py2.py3-none-any.whl
%global python3_record %{python3_sitelib}/%{srcname}-%{version}.dist-info/RECORD
%endif

Name:           python3-setuptools
Version:        19.6.2
Release:        1%{?dist}
Summary:        Easily build and distribute Python 3 packages

# LIcensing is in flux, see https://bitbucket.org/pypa/setuptools/issues/132/missing-license
License:        MIT and ASL 2.0
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://pypi.python.org/packages/source/s/%{srcname}/%{srcname}-%{version}.tar.gz
# ASL 2.0
Source1:        http://www.apache.org/licenses/LICENSE-2.0

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
%if 0%{?with_check}
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-mock
%endif # with_check
%if 0%{?build_wheel}
BuildRequires:  python%{python3_pkgversion}-pip
BuildRequires:  python%{python3_pkgversion}-wheel
%endif # build_wheel

%description
Setuptools is a collection of enhancements to the Python 3 distutils that allow
you to more easily build and distribute Python 3 packages, especially ones that
have dependencies on other packages.

This package also contains the runtime components of setuptools, necessary to
execute the software that requires pkg_resources.py.


%package -n python%{python3_pkgversion}-setuptools
Summary:        Easily build and distribute Python %{python3_pkgversion} packages
Group:          Applications/System
%{?python_provide:%python_provide python3-setuptools}

%description -n python%{python3_pkgversion}-setuptools
Setuptools is a collection of enhancements to the Python %{python3_pkgversion} distutils that allow
you to more easily build and distribute Python %{python3_pkgversion} packages, especially ones that
have dependencies on other packages.

This package also contains the runtime components of setuptools, necessary to
execute the software that requires pkg_resources.py.


%prep
%setup -q -n %{srcname}-%{version}

# We can't remove .egg-info (but it doesn't matter, since it'll be rebuilt):
#  The problem is that to properly execute setuptools' setup.py,
#   it is needed for setuptools to be loaded as a Distribution
#   (with egg-info or .dist-info dir), it's not sufficient
#   to just have them on PYTHONPATH
#  Running "setup.py install" without having setuptools installed
#   as a distribution gives warnings such as
#    ... distutils/dist.py:267: UserWarning: Unknown distribution option: 'entry_points'
#   and doesn't create "easy_install" and .egg-info directory
# Note: this is only a problem if bootstrapping wheel or building on RHEL,
#  otherwise setuptools are installed as dependency into buildroot

# Strip shbang
find setuptools -name \*.py | xargs sed -i -e '1 {/^#!\//d}'
# Remove bundled exes
rm -f setuptools/*.exe
# These tests require internet connection
rm setuptools/tests/test_integration.py 


%build
%if 0%{?build_wheel}
%{__python3} setup.py bdist_wheel
%else
%{__python3} setup.py build
%endif


%install
%if 0%{?build_wheel}
pip%{python3_version} install -I dist/%{python3_wheelname} --root %{buildroot} --strip-file-prefix %{buildroot}

sed -i '/\/usr\/bin\/easy_install,/d' %{buildroot}%{python3_record}
%else
%{__python3} setup.py install --skip-build --root %{buildroot}
%endif
rm %{buildroot}%{_bindir}/easy_install

rm -rf %{buildroot}%{python3_sitelib}/setuptools/tests
%if 0%{?build_wheel}
sed -i '/^setuptools\/tests\//d' %{buildroot}%{python3_record}
%endif

install -p -m 0644 %{SOURCE1} asl.txt
find %{buildroot}%{python3_sitelib} -name '*.exe' | xargs rm -f

# Don't ship these
rm -r docs/{Makefile,conf.py,_*}


%if 0%{?with_check}
%check
LANG=en_US.utf8 PYTHONPATH=$(pwd) py.test-%{python3_version}
%endif


%files -n python%{python3_pkgversion}-setuptools
%license asl.txt
%doc docs/*
%{python3_sitelib}/easy_install.py
%{python3_sitelib}/_markerlib/
%{python3_sitelib}/pkg_resources/
%{python3_sitelib}/setuptools*/
%{python3_sitelib}/__pycache__/*
%{_bindir}/easy_install-3.*


%changelog
* Wed Feb 3 2016 Orion Poplawski <orion@cora.nwra.com> - 19.6.2-1
- Update to 19.6.2
- Update license
- Fix python3 package file ownership

* Wed Dec 30 2015 Orion Poplawski <orion@cora.nwra.com> - 19.2-3
- Cleanup docs
- Add version info to summary and description

* Wed Dec 30 2015 Orion Poplawski <orion@cora.nwra.com> - 19.2-2
- Drop group tag
- Add bootstrap conditional
- Use specific pip version
- Use %%license
- Update license and license source
- Strip unneeded shbangs

* Tue Dec 29 2015 Orion Poplawski <orion@cora.nwra.com> - 19.2-1
- Update to 19.2

* Tue Dec 29 2015 Orion Poplawski <orion@cora.nwra.com> - 19.1.1-1
- Initial EPEL package
